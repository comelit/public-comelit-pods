Pod::Spec.new do |spec|

  spec.name         = "CGModule"
  spec.version      = "0.6.6"
  spec.summary      = "Comelit module to integrate Vip functionalities"

  spec.description  = <<-DESC
                      The CGModule allows an app to connect and interact with a Comelit
                      Vip system.
                      It supports the following Vip devices: 1456,1456B,1456S,6741w,6742w and 6842w
                      It supports push notification and allow call ui customization.
                      DESC

  spec.homepage     = "https://www.comelitgroup.com/"

  spec.license      = { :type => "Copyright", :file => "LICENSE" }

  spec.author       = { "Cornelli Fabio" => "fabio.cornelli@comelit.it" }

  spec.platform     = :ios, "12.0"
  spec.source       = { :http => "https://artifactory.cloud.comelitgroup.com/artifactory/framework-release-3rd/com/comelitgroup/module/#{spec.version}/#{spec.name}.zip" }

  spec.preserve_paths = 'CGModule.xcframework','CMGMultimedia.xcframework','ComelitVipKit.xcframework',
                        'ComelitCoreKit.xcframework','CMGTolo.xcframework','VipComelit.xcframework',
                        'ffmpeg.xcframework','openssl.xcframework','pjproject.xcframework','curl.xcframework','LICENSE'

  spec.vendored_frameworks = 'CGModule.xcframework','CMGMultimedia.xcframework','ComelitVipKit.xcframework',
                             'ComelitCoreKit.xcframework','CMGTolo.xcframework','VipComelit.xcframework',
                             'ffmpeg.xcframework','openssl.xcframework','pjproject.xcframework','curl.xcframework'

  spec.frameworks = "CoreMedia", "UserNotifications", "VideoToolBox", "CFNetwork"

  spec.dependency "CocoaAsyncSocket", "~> 7.6"
  spec.dependency "CocoaLumberjack", "~> 3.7.0"
  spec.dependency "FMDB", "~> 2.7.5"
  spec.dependency "RxSwift", "~> 6.1"
  spec.dependency "TPCircularBuffer", "1.6.1"

  #remove user_target_xconfig when https://github.com/CocoaPods/CocoaPods/issues/9232 will be solved
  spec.user_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  spec.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
end
