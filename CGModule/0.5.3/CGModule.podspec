Pod::Spec.new do |spec|

  spec.name         = "CGModule"
  spec.version      = "0.5.3"
  spec.summary      = "Comelit module to integrate Vip functionalities"

  spec.description  = <<-DESC
                      The CGModule allows an app to connect and interact with a Comelit
                      Vip system.
                      It supports the following Vip devices: 1456,1456B,1456S,6741w,6742w and 6842w
                      It supports push notification and allow call ui customization.
                      DESC

  spec.homepage     = "https://www.comelitgroup.com/"

  spec.license      = { :type => "Copyright", :file => "LICENSE" }

  spec.author       = { "Cornelli Fabio" => "fabio.cornelli@comelit.it" }

  spec.platform     = :ios, "10.0"
  spec.source       = { :http => "https://artifactory.cloud.comelitgroup.com/artifactory/framework-release-3rd/com/comelitgroup/module/#{spec.version}/#{spec.name}.zip" }

  spec.preserve_paths = 'CGModule.framework','ComelitVipKit.framework',
                        'ComelitCoreKit.framework','CMGTolo.framework','VipComelit.framework',
                        'ffmpeg.framework','openssl.framework','pjproject.framework','LICENSE'

  spec.vendored_frameworks = 'CGModule.framework','ComelitVipKit.framework',
                             'ComelitCoreKit.framework','CMGTolo.framework','VipComelit.framework',
                             'ffmpeg.framework','openssl.framework','pjproject.framework'

  spec.frameworks = "CoreMedia", "UserNotifications", "VideoToolBox", "CFNetwork"

  spec.dependency "CocoaAsyncSocket", "~> 7.6"
  spec.dependency "CocoaLumberjack", "~> 3.4.2"
  spec.dependency "FMDB", "~> 2.7.5"
  spec.dependency "SystemServices", "~> 2.0.0"
end
